# Time Tracker

Проект-пример для демонстрации возможностей Django на онлайн курсе для Казанского федерального университета.

[Версия с кастомным юзером находится в ветке **old_main**](https://gitlab.com/atnartur.uni/django-2k-11-200/-/tree/old_main?ref_type=heads)

## Запуск проекта для разработки

- `poetry shell` - войти в виртуальное окружение
- `poetry install` - установка зависимостей
- `docker-compose up -d` - запустить дополнительные сервисы в Docker. Если у Вас нет Docker, установите
  - [PostgreSQL](https://www.postgresql.org/)
- `python src/manage.py migrate` - примененить миграции
- `python src/manage.py runserver` - запустить сервер для разработки на http://127.0.0.1:8000
- `pip install pre-commit` - установка pre-commit hooks
- `pre-commit run -a` - прогнать все линтеры
- `poetry env info` - получить информацию о виртуальном окружении (например, для настройки интерпретатора)
- `./deploy/run_workers.sh` - запустить celery worker

&copy; 2024 [Артур Атнагулов](https://atnartur.dev)
