#!/bin/bash

# Fix MacOS workers start
# https://github.com/rails/rails/issues/38560#issuecomment-590236052
export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES

cd src

celery -A timetracker worker -B -l INFO -Q default,notifications