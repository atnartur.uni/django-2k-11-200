from django_filters import rest_framework as filters
from web.models import TimeSlot


class TimeslotFilters(filters.FilterSet):
    start_date = filters.DateTimeFilter(field_name="start_date", lookup_expr="gte")
    end_date = filters.DateTimeFilter(field_name="start_date", lookup_expr="lte")

    class Meta:
        model = TimeSlot
        fields = ("start_date", "end_date", "is_realtime")
