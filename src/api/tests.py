import pytest
from rest_framework.test import RequestsClient


@pytest.fixture
def api_client():
    return RequestsClient()


def test_main(api_client):
    response = api_client.get("http://testserver/api")
    assert response.status_code == 200
    assert response.json()["status"] == "ok"
