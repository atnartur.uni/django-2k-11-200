from rest_framework.decorators import api_view, permission_classes
from rest_framework.mixins import ListModelMixin, CreateModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, GenericViewSet
from drf_yasg.utils import swagger_auto_schema
from django_filters import rest_framework as filters

from api.filters import TimeslotFilters
from api.serializers import (
    StatusSerializer,
    TimeSlotSerializer,
    TagSerializer,
    UserSerializer,
)
from web.models import TimeSlot, TimeSlotTag


@swagger_auto_schema(
    method="get", operation_summary="api test", responses={200: StatusSerializer}
)
@api_view(["GET"])
@permission_classes([])
def main_view(request):
    """API for server testing"""
    return Response(StatusSerializer({"status": "ok"}).data)


@swagger_auto_schema(method="get", responses={200: UserSerializer})
@api_view(["GET"])
def user_view(request):
    return Response(UserSerializer(request.user).data)


class TimeslotModelViewSet(ModelViewSet):
    serializer_class = TimeSlotSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = TimeslotFilters

    def get_queryset(self):
        return (
            TimeSlot.objects.all()
            .select_related("user")
            .prefetch_related("tags")
            .filter(user=self.request.user)
        )


class TagsViewSet(ListModelMixin, CreateModelMixin, GenericViewSet):
    """Tags API"""

    serializer_class = TagSerializer

    def get_queryset(self):
        return TimeSlotTag.objects.all().filter(user=self.request.user)
