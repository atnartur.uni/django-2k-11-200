from django.core.management.base import BaseCommand

from bot_handlers.services.bot import listen_bot_notifications


class Command(BaseCommand):
    def handle(self, *args, **options):
        listen_bot_notifications()
