from django.contrib.auth import get_user_model
from django.db import models


User = get_user_model()


class TelegramUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    telegram_id = models.CharField(max_length=50)


class TelegramAuthHash(models.Model):
    telegram_id = models.CharField(max_length=50, unique=True)
    hash = models.CharField(max_length=100)


class TelegramStartHash(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    hash = models.CharField(max_length=100)
