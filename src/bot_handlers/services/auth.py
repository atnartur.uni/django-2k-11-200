from uuid import uuid4

from django.conf import settings
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from django.urls import reverse
from bot_handlers.models import TelegramAuthHash, TelegramStartHash, TelegramUser


def generate_auth_hash_link(chat_id: int) -> str:
    hash = str(uuid4())
    TelegramAuthHash.objects.update_or_create(
        telegram_id=chat_id, defaults=dict(hash=hash)
    )
    return f"{settings.HOST_URL}{reverse('telegram_auth_hash', args=(hash,))}"


def connect_auth_hash_with_user(auth_hash: str, user_id: int) -> User | None:
    telegram_auth_hash = TelegramAuthHash.objects.filter(hash=auth_hash).first()
    if telegram_auth_hash is None:
        return None
    TelegramUser.objects.update_or_create(
        user_id=user_id,
        defaults=dict(telegram_id=telegram_auth_hash.telegram_id),
    )
    telegram_auth_hash.delete()
    return telegram_auth_hash.telegram_id


def generate_telegram_start_link(user_id: int) -> str:
    hash = str(uuid4())
    TelegramStartHash.objects.update_or_create(
        user_id=user_id,
        defaults=dict(hash=hash),
    )
    return f"https://t.me/{settings.TELEGRAM_BOT_LOGIN}?start={hash}"


def connect_start_hash_with_telegram(auth_hash: str, chat_id: int) -> User | None:
    hash_info = TelegramStartHash.objects.filter(hash=auth_hash).first()
    if hash_info is None:
        return None
    TelegramUser.objects.update_or_create(
        user=hash_info.user, defaults=dict(telegram_id=chat_id)
    )
    hash_info.delete()
    return hash_info.user
