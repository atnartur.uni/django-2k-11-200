import telebot

from django.conf import settings

from bot_handlers.services.auth import (
    connect_start_hash_with_telegram,
    generate_auth_hash_link,
)

bot = telebot.TeleBot(settings.TELEGRAM_BOT_TOKEN, parse_mode=None)


@bot.message_handler(commands=["start"])
def send_welcome(message: telebot.types.Message):
    message_lst = message.text.split(" ")
    if len(message_lst) > 1:
        auth_hash = message_lst[1]
        user = connect_start_hash_with_telegram(auth_hash, message.chat.id)
        if user:
            bot.reply_to(message, f"Вы авторизованы как {user.username}")
            return

    url = generate_auth_hash_link(message.chat.id)
    bot.reply_to(message, f"Привяжите аккаунт по ссылке: {url}")


def listen_bot_notifications():
    print("start bot")
    bot.infinity_polling()
