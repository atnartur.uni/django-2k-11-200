from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect

from bot_handlers.services.auth import (
    connect_auth_hash_with_user,
    generate_telegram_start_link,
)
from bot_handlers.services.bot import bot


@login_required
def telegram_auth_view(request, auth_hash):
    telegram_id = connect_auth_hash_with_user(auth_hash, request.user.id)
    bot.send_message(telegram_id, f"Привязан аккаунт {request.user.username}")
    return redirect("main")


@login_required
def telegram_start_view(request):
    return redirect(generate_telegram_start_link(request.user.id))
