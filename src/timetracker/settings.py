"""
Django settings for timetracker project.

Generated by 'django-admin startproject' using Django 4.1.5.

For more information on this file, see
https://docs.djangoproject.com/en/4.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/4.1/ref/settings/
"""
import os
from pathlib import Path

from dotenv import load_dotenv

load_dotenv()

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/4.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "django-insecure-ie5qid#+5+mc_&s**djb7uw6*$jznz9=ck)r@k5o)egye(howp"

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "drf_yasg",
    "rest_framework",
    "rest_framework.authtoken",
    "web",
    "api",
    "bot_handlers",
    "corsheaders",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    # "timetracker.middlewares.SqlPrintingMiddleware",
    "timetracker.middlewares.StatMiddleware",
]

ROOT_URLCONF = "timetracker.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "timetracker.context_processors.project",
            ],
        },
    },
]

WSGI_APPLICATION = "timetracker.wsgi.application"


# Database
# https://docs.djangoproject.com/en/4.1/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": os.environ.get("DB_NAME", "timetracker"),
        "USER": os.environ.get("DB_USER", "timetracker"),
        "PASSWORD": os.environ.get("DB_PASSWORD", "timetracker"),
        "HOST": os.environ.get("DB_HOST", "localhost"),
        "PORT": 5432,
    }
}

REDIS_HOST = os.environ.get("REDIS_HOST", "127.0.0.1")
REDIS_DB = os.environ.get("REDIS_DB", 0)
REDIS_PORT = 6379
REDIS_CONNECTION_STRING = f"redis://{REDIS_HOST}:{REDIS_PORT}/{REDIS_DB}"
CACHE_BACKEND = os.environ.get("CACHE_BACKEND", "django_redis.cache.RedisCache")

CACHES = {
    "default": {
        "BACKEND": CACHE_BACKEND,
        "LOCATION": REDIS_CONNECTION_STRING,
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        },
    }
}


# Password validation
# https://docs.djangoproject.com/en/4.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]


# Internationalization
# https://docs.djangoproject.com/en/4.1/topics/i18n/

LANGUAGE_CODE = "ru-ru"

TIME_ZONE = "Europe/Moscow"

USE_I18N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/4.1/howto/static-files/

STATIC_URL = "static/"

# Default primary key field type
# https://docs.djangoproject.com/en/4.1/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

MEDIA_ROOT = "media/"
MEDIA_URL = "media/"

STATIC_ROOT = "static"

LOGIN_URL = "auth"
LOGIN_REDIRECT_URL = "main"

REST_FRAMEWORK = {
    "DEFAULT_AUTHENTICATION_CLASSES": [
        "rest_framework.authentication.SessionAuthentication",
        "rest_framework.authentication.TokenAuthentication",
    ],
    "DEFAULT_PERMISSION_CLASSES": ["rest_framework.permissions.IsAuthenticated"],
    "DEFAULT_PAGINATION_CLASS": "rest_framework.pagination.LimitOffsetPagination",
    "PAGE_SIZE": 10,
}

TELEGRAM_BOT_TOKEN = os.environ.get("TELEGRAM_BOT_TOKEN")
TELEGRAM_BOT_LOGIN = os.environ.get("TELEGRAM_BOT_LOGIN", "itistestbot")

HOST_URL = os.environ.get("HOST_URL", "http://localhost:8000")

EMAIL_HOST = os.environ.get("EMAIL_HOST")
EMAIL_PORT = os.environ.get("EMAIL_PORT")
EMAIL_HOST_USER = os.environ.get("EMAIL_HOST_USER")
EMAIL_HOST_PASSWORD = os.environ.get("EMAIL_HOST_PASSWORD")
DEFAULT_FROM_EMAIL = os.environ.get("DEFAULT_FROM_EMAIL", EMAIL_HOST_USER)
EMAIL_USE_SSL = True

CELERY_BROKER_URL = os.environ.get("CELERY_BROKER_URL", REDIS_CONNECTION_STRING)
CELERY_BEAT_SCHEDULE = {
    "add-every-30-seconds": {
        "task": "web.tasks.periodic_example_task",
        "schedule": 30.0,
        "args": (16, 16),
    },
}
CELERY_TIMEZONE = "UTC"
CELERY_TASK_ALWAYS_EAGER = (
    os.environ.get("CELERY_TASK_ALWAYS_EAGER", "false").lower() == "true"
)
CELERY_TASK_EAGER_PROPAGATES = (
    os.environ.get("CELERY_TASK_EAGER_PROPAGATES", "false").lower() == "true"
)

CORS_ALLOWED_ORIGINS = ["http://localhost:5173", "http://localhost:3000"]

RECORD_PAGE_STAT = os.environ.get("RECORD_PAGE_STAT", "true").lower() == "true"
