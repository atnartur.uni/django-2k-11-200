import json
from django.core.management.base import BaseCommand

from bot_handlers.models import User
from web.tasks import test_task


class Command(BaseCommand):
    def handle(self, *args, **options):
        user = User.objects.first()
        print(test_task.delay(user.id))
