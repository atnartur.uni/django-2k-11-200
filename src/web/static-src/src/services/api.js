export async function getApiStatus() {
    const response = await fetch('/api/');
    return await response.json();
}