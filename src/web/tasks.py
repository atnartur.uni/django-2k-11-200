from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.mail import send_mail
from django.template.loader import render_to_string
from timetracker.celery import app

User = get_user_model()


@app.task(queue="default")
def test_task(user_id):
    user = User.objects.get(id=user_id)
    print("hello world", user.username)


@app.task(queue="notifications")
def send_registration_email(user_id):
    user = User.objects.get(id=user_id)
    html_content = render_to_string(
        "web/emails/register_email.html", {"username": user.username}
    )

    send_mail(
        "Time Tracker - регистрация",
        "",
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[user.email],
        html_message=html_content,
        fail_silently=False,
    )


@app.task(queue="default")
def periodic_example_task(a, b):
    print("I STARTED!", a, b)
