import pytest

from web.tests.factories import UserFactory


@pytest.fixture
def user():
    return UserFactory()
