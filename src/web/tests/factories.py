import factory

from web.models import User
from web.models import TimeSlot


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.faker.Faker("name")
    email = factory.faker.Faker("email")


class TimeSlotFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = TimeSlot

    title = factory.faker.Faker("word")
    user = factory.SubFactory(UserFactory)
