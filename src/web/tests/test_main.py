from django.urls import reverse

from web.tests.factories import UserFactory


def test_open_main_page_without_auth(client):
    response = client.get(reverse("main"))
    assert response.status_code == 302
    assert "auth" in response.headers["Location"]


def test_open_main_page_with_auth(client):
    client.force_login(UserFactory())
    response = client.get(reverse("main"))
    assert response.status_code == 200
