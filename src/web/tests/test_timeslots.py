from django.urls import reverse
import pytest

from web.models import TimeSlot
from web.tests.factories import TimeSlotFactory, UserFactory


@pytest.fixture(autouse=True)
def user(client):
    user = UserFactory()
    client.force_login(user)
    return user


def test_empty_list(client):
    response = client.get(reverse("main"))
    assert response.status_code == 200
    assert "всего 0 записей" in response.content.decode()


def test_not_empty_list(client, user):
    TimeSlotFactory(user=user)
    response = client.get(reverse("main"))
    assert response.status_code == 200
    assert "всего 1 записей" in response.content.decode()


def test_create(client, user):
    response = client.post(
        reverse("time_slots_add"),
        {"title": "created timeslot", "start_date": "2023-12-26T16:17"},
        follow=True,
    )
    assert response.status_code == 200
    assert "Слот успешно сохранен!" in response.content.decode()
    assert TimeSlot.objects.filter(user=user, title="created timeslot").count() == 1
