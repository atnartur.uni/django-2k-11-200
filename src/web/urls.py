from django.urls import path
from django.views.generic import RedirectView

from web.views import (
    LogoutView,
    ImportFormView,
    TimeslotDetailView,
    main_view,
    registration_view,
    auth_view,
    time_slot_edit_view,
    tags_view,
    tags_delete_view,
    holidays_delete_view,
    holidays_view,
    time_slot_delete_view,
    time_slot_stop_view,
    analytics_view,
    stat_view,
)

urlpatterns = [
    path("", main_view, name="main"),
    path("admin_panel/", RedirectView.as_view(url="/admin")),
    path("import/", ImportFormView.as_view(), name="import"),
    path("analytics/", analytics_view, name="analytics"),
    path("stat/", stat_view, name="stat"),
    path("registration/", registration_view, name="registration"),
    path("auth/", auth_view, name="auth"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("time_slots/add/", time_slot_edit_view, name="time_slots_add"),
    path("time_slots/<int:id>/", time_slot_edit_view, name="time_slots_edit"),
    path(
        "time_slots/<int:pk>/view/", TimeslotDetailView.as_view(), name="time_slot_view"
    ),
    path(
        "time_slots/<int:id>/delete/", time_slot_delete_view, name="time_slots_delete"
    ),
    path("time_slots/<int:id>/stop/", time_slot_stop_view, name="time_slots_stop"),
    path("tags/", tags_view, name="tags"),
    path("tags/<int:id>/delete/", tags_delete_view, name="tags_delete"),
    path("holidays/", holidays_view, name="holidays"),
    path("holidays/<int:id>/delete/", holidays_delete_view, name="holidays_delete"),
]
